﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Data.SqlClient;

namespace sql_z_txt
{
    public partial class Form1 : Form
    {

        Engine metods;
      
        public Form1()
        {
            InitializeComponent();
            metods = new Engine(Convert.ToString(comboBox1.SelectedItem));
        }

        private void button1_Click(object sender, EventArgs e)
        {
            metods.OpenFileTxt();
        }


        private void button2_Click(object sender, EventArgs e)
        {          
            metods.GetToSql();
            MessageBox.Show("Zaimplementowano dane do bazy SQL", "INFORMACJA", MessageBoxButtons.OK);

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            metods.Profiles = comboBox1.SelectedItem.ToString();
        }

    }
}

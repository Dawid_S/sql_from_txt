﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace sql_z_txt
{
    public class Engine
    {

        OpenFileDialog ofd;


        public Engine()
        {
            
        }

        public Engine(string baza)
        {
            Profiles = baza;
        }

        public string Profiles { get; set;}


        public void OpenFileTxt()
        {
            //pełna scieżka dostepu do IPN
            // otwarcie połaczenia z txt
            ofd = new OpenFileDialog();
            ofd.Filter = "Pliki TXT|*.txt";
            ofd.ShowDialog();

            //string path = Environment.CurrentDirectory + "\\IPN.txt";
            //StreamReader sr = new StreamReader(path); 
        }

        public void GetToSql()
        {
         //Czytana linia
            string line = null;
            using (StreamReader sr = new StreamReader(ofd.FileName))
            {
                while ((line = sr.ReadLine()) != null)
                {
                                      
                    SqlConnection connection = new SqlConnection("Server=DAWID-KOMPUTER\\SQLEXPRESS;DataBase=Profile;Integrated Security=True;Connect Timeout=30");
                    connection.Open();
                   
                    SqlCommand cmd = new SqlCommand(GetInsert(line), connection); //(cmd - co się ma wykonać, sql)
                    cmd.ExecuteNonQuery(); //Wykonaj niepytające
                }
            }
        }

        public string GetInsert(string line)
        {          
            line = line.Replace(',', '.');//Insertowanie liszb SQL przyjmuje "."                                           
            string[] columns = line.Split(new char[] {'\t'}, StringSplitOptions.None); //StringSplitOption.None - nie usuwa wolnych miejc
            return string.Format(@"insert into {15}  (G, h, b, tw, tf, A, Wely, Wply, Ly, iy, Welz, Wplz, Lz, iz, ID)
                                      values ({0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}, '{9}', {10}, {11}, {12}, {13}, '{14}')",
                                       columns[0],
                                       columns[1],
                                       columns[2],
                                       columns[3],
                                       columns[4],
                                       columns[5],
                                       columns[6],
                                       columns[7],
                                       columns[8],
                                       columns[9],
                                       columns[10],
                                       columns[11],
                                       columns[12],
                                       columns[13],
                                       columns[14],
                                       Profiles );
        }
    }
}
